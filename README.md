# README

Set up development env:
* bin/rails db:migrate
* bin/rails db:seed
* bin/rails server

Set up testing env:

* bin/rails db:migrate RAILS_ENV=test
* bin/rails test
