require "test_helper"

class PokemonsControllerTest < ActionDispatch::IntegrationTest

  setup do
    @valid_attributes = { name: "Pikachu", type_1: "Electric", total: 320, hp: 35, attack: 55, defense: 40, sp_attack: 50, sp_def: 50, speed: 90, generation: 1, legendary: false }
    @pokemon = pokemons(:Bulbasaur)
  end

  test "should get index" do
    get pokemons_url, as: :json
    assert_response :success
  end

  test "should return first page of the pokemon list" do
    get pokemons_url, params: { page: 1, per_page: 1 }
    json_response = JSON.parse(response.body)
    assert_response :success
    assert_equal 1, json_response['data'].count
    assert_equal 1, json_response['current_page']
    assert_equal 2, json_response['total_pages']
  end

  test "should return second page of the pokemon list" do
    get pokemons_url, params: { page: 2, per_page: 1 }
    json_response = JSON.parse(response.body)
    assert_response :success
    assert_equal 1, json_response['data'].count
    assert_equal 2, json_response['current_page']
    assert_equal 2, json_response['total_pages']
  end

  test "should get the first pokemon ordered by name" do
    get pokemons_url, params: { page: 1, per_page: 1, order_by: 'name' }
    first_pokemon = Pokemon.order('name').first
    assert_response :success
    assert_equal JSON.parse(response.body)['data'].first['name'], first_pokemon.name
  end

  test "should create pokemon" do
    assert_difference("Pokemon.count") do
      post pokemons_url, params: { pokemon: @valid_attributes }, as: :json
    end
    assert_response :created
  end

  test "should raise an error on creating a pokemon with invalid params" do
    assert_raises ActionController::ParameterMissing do
      post pokemons_url, params: { pokemon: {} }, as: :json
    end
  end

  test "should show pokemon" do
    get pokemon_url(@pokemon), as: :json
    assert_response :success
  end

  test "should update pokemon" do
    patch pokemon_url(@pokemon), params: { pokemon: @valid_attributes }, as: :json
    assert_response :success
  end

  test "should destroy pokemon" do
    assert_difference("Pokemon.count", -1) do
      delete pokemon_url(@pokemon), as: :json
    end
    assert_response :no_content
  end

end
