require "test_helper"

class PokemonTest < ActiveSupport::TestCase

  setup do
    @pokemon = pokemons(:Bulbasaur)
  end
  test 'Should be invalid without any attributes' do
    assert_not (Pokemon.new).valid?
  end

  test 'Should be valid with all required attributes' do
    assert @pokemon.valid?
  end

  test 'Should be invalid with duplicate name' do
    assert_not (Pokemon.new(name: @pokemon.name)).valid?
  end

  test 'validate name length' do
    @pokemon.name = 'Pi'
    refute @pokemon.valid?
    @pokemon.name = 'a' * 101
    refute @pokemon.valid?
    @pokemon.name = 'pipf'
    assert @pokemon.valid?
  end

  test 'validate type_2 length' do
    @pokemon.type_2 = 'a' * 101
    refute @pokemon.valid?
    @pokemon.type_2 = 'Normal'
    assert @pokemon.valid?
    @pokemon.type_2 = nil
    assert @pokemon.valid?
  end

  test 'validate hp, attack, defense, sp_def, speed ' do
    stats = [:hp, :attack, :defense, :sp_attack, :sp_def, :speed]
    stats.each do |stat|
      @pokemon[stat] = 'a'
      refute @pokemon.valid?
      @pokemon[stat] = 10000
      refute @pokemon.valid?
      @pokemon[stat] = -1
      refute @pokemon.valid?
      @pokemon[stat] = 1
      assert @pokemon.valid?
      @pokemon[stat] = 10
      assert @pokemon.valid?
      @pokemon[stat] = 50
      assert @pokemon.valid?
      @pokemon[stat] = 100
      assert @pokemon.valid?
      @pokemon[stat] = 1000
      assert @pokemon.valid?
    end
  end

  test 'validate generation' do
    @pokemon.generation = 'a'
    refute @pokemon.valid?
    @pokemon.generation = nil
    refute @pokemon.valid?
    @pokemon.generation = 10
    refute @pokemon.valid?
    @pokemon.generation = 9
    assert @pokemon.valid?
    @pokemon.generation = 0
    assert @pokemon.valid?
  end

  test 'Should be valid with all required attributes and a falsy legendary status' do
    @pokemon.legendary = false
    assert @pokemon.valid?
  end

  test 'Should be valid with all required attributes and a truthy legendary status' do
    @pokemon.legendary = true
    assert @pokemon.valid?
  end

end
