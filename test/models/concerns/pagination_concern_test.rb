require 'test_helper'

class PaginationConcernTest < ActiveSupport::TestCase

  test "should paginate records" do
    50.times do |i|
      TestModel.create(name: "Test #{i}")
    end

    expected_total = 50
    expected_total_pages = 5
    expected_per_page = 10
    expected_current_page = 1

    # Act
    result = TestModel.paginate(1, 10)

    # Assert
    assert_equal expected_total, result[:total]
    assert_equal expected_total_pages, result[:total_pages]
    assert_equal expected_per_page, result[:per_page]
    assert_equal expected_current_page, result[:current_page]
    assert_nil result[:prev_page]
    assert_equal 2, result[:next_page]
    assert result[:is_first_page]
    refute result[:is_last_page]
    refute result[:is_out_of_range]
  end
end
