require 'test_helper'

class OrderingConcernTest < ActiveSupport::TestCase

  setup do
    10.times do |i|
      TestModel.create(name: "Test #{i}")
    end
  end

  test "should order records" do
    order_by = 'name'
    order_sort = 'desc'

    firstModel = TestModel.order(name: :desc).limit(1).first()
    firstOrderedModel = TestModel.orderBy(order_by, order_sort).limit(1).first()

    assert_equal firstOrderedModel, firstModel
  end

  test "should order records by ASC id with invalid order_by column" do
    order_by = 'foo_column'
    order_sort = 'desc'

    firstModel = TestModel.order(id: :asc).limit(1).first()
    firstOrderedModel = TestModel.orderBy(order_by, order_sort).limit(1).first()

    assert_equal firstOrderedModel, firstModel
  end

  test "should order records asc by valid order_by column & invalid sort_by" do
    order_by = 'name'
    order_sort = 'foo_sort'

    firstModel = TestModel.order(name: :asc).limit(1).first()
    firstOrderedModel = TestModel.orderBy(order_by, order_sort).limit(1).first()

    assert_equal firstOrderedModel, firstModel
  end

  test "should order records by id with empty order_by & sort_by values provided" do
    order_by = ''
    order_sort = ''

    firstModel = TestModel.order(id: :asc).limit(1).first()
    firstOrderedModel = TestModel.orderBy(order_by, order_sort).limit(1).first()

    assert_equal firstOrderedModel, firstModel
  end
end