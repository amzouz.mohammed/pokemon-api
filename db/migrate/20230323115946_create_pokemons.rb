class CreatePokemons < ActiveRecord::Migration[7.0]
  def change
    create_table :pokemons do |t|
      t.string :name, index: { unique: true }, :null => false, :limit => 100
      t.string :type_1, index: true, :null => false, :limit => 100
      t.string :type_2, index: true, :limit => 100, default: nil
      t.integer :total, :unsigned => true, :null => false, :limit => 3
      t.integer :hp, :unsigned => true, :null => false, :limit => 3
      t.integer :attack, :unsigned => true, :null => false, :limit => 3
      t.integer :defense, :unsigned => true, :null => false, :limit => 3
      t.integer :sp_attack, :unsigned => true, :null => false, :limit => 3
      t.integer :sp_def, :unsigned => true, :null => false, :limit => 3
      t.integer :speed, :unsigned => true, :null => false, :limit => 3
      t.integer :generation, :unsigned => true, :null => false, :limit => 3
      t.boolean :legendary, :null => false
      t.timestamps
    end
  end
end
