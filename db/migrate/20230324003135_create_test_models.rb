class CreateTestModels < ActiveRecord::Migration[7.0]
  def change
    if Rails.env.test?
      create_table :test_models, env: 'test' do |t|
        t.string :name
        t.timestamps
      end
    end
  end
end
