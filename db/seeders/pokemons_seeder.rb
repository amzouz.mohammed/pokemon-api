require 'csv'

Pokemon.delete_all

rows = []
CSV.foreach(Rails.root.join('db', 'datas', 'pokemons.csv'), headers: true) do |row|
  rows << row.to_h
  if rows.length == 250
    Pokemon.import rows;
    rows = []
  end
end

# Process any remaining rows that didn't fit into a full batch
if rows.length > 0
  Pokemon.import rows;
end