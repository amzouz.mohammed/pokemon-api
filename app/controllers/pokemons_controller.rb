class PokemonsController < ApplicationController
  before_action :set_pokemon, only: %i[ show update destroy ]

  def index
    render json: Pokemon.orderBy(params[:order_by], params[:order_sort]).paginate(params[:page], params[:per_page]), status: :ok
  end

  def show
    render json: @pokemon, status: :ok
  end

  def create
    pokemon = Pokemon.new(pokemon_params)
    if pokemon.save
      render json: pokemon, status: :created
    else
      render json: pokemon.errors, status: :unprocessable_entity
    end
  end

  def update
    if @pokemon.update(pokemon_params)
      render json: @pokemon
    else
      render json: @pokemon.errors, status: :unprocessable_entity
    end
  end

  def destroy
    @pokemon.destroy
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_pokemon
    @pokemon = Pokemon.find(params[:id])
  end

  # Only allow a list of trusted parameters through.
  def pokemon_params
    params.require(:pokemon)
          .permit(:name, :type_1, :total, :hp, :attack, :defense, :sp_attack, :sp_def, :speed, :generation, :legendary)
  end
end
