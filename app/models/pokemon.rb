class Pokemon < ApplicationRecord
  include PaginationConcern
  include OrderingConcern

  validates :name, :type_1, :total, :hp, :attack, :defense, :sp_attack, :sp_def, :speed, :generation, presence: true
  validates :name, uniqueness: true, length: { in: 3..100 }
  validates :type_1, length: { in: 3..100 }
  validates :type_2, length: { maximum: 100 }
  validates :total, :hp, :attack, :defense, :sp_attack, :sp_def, :speed, numericality: { :greater_than_or_equal_to => 0, less_than_or_equal_to: 1000, only_integer: true }
  validates :generation, numericality: { :greater_than_or_equal_to => 0, less_than: 10, only_integer: true }
  validates :legendary, inclusion: { in: [true, false] }
end
