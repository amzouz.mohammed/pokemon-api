module PaginationConcern
  extend ActiveSupport::Concern

  included do
    paginates_per 10
    max_paginates_per 20
  end

  class_methods do
    def paginate(page, per_page)
      records = page(page).per(per_page)
      return {
        data: records,
        total: records.total_count,
        total_pages: records.total_pages,
        per_page: records.limit_value,
        current_page: records.current_page,
        prev_page: records.prev_page,
        next_page: records.next_page,
        is_first_page: records.first_page?,
        is_last_page: records.last_page?,
        is_out_of_range: records.out_of_range?,
      }
    end
  end
end