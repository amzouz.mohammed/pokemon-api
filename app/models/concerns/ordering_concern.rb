module OrderingConcern
  extend ActiveSupport::Concern

  class_methods do
    def orderBy(order_by, order_sort)
      order_by = (column_names.include? order_by) ? order_by : nil
      order_sort = (%w[asc desc].include? order_sort) && order_by ? order_sort : nil
      return order("#{order_by} #{order_sort}")
    end
  end
end